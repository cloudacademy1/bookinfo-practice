const http = require('http')

const server = http.createServer()
server.on('request', (req, res) => {
  res.statusCode = 200
  console.log(`I am ${process.env.NAME} and I received a new request from ${req.headers.host}`);
  res.end(`<h1>${Math.floor(Math.random() * 100)}</h1>`)
})

server.listen(8080, () => console.log(`Server running at port 8080`))
